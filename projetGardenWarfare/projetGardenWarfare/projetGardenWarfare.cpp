// ProjetPvsZ.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <ctime>
#include <iostream>
#include <cstdlib>
#include <SFML/Graphics.hpp>
#include "Zombie.h"
#include "Entity.h"
#include "Peashooter.h"
#include "Projectile.h"
#include "Game.h"



int main()
{
	//Starting of the game
	Game game;
	game.run();

	return EXIT_SUCCESS;
}

