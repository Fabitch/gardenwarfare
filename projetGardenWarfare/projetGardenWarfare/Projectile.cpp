#include "stdafx.h"
#include "Projectile.h"


Projectile::Projectile()
{
	rect.setSize(sf::Vector2f(20, 20));
	rect.setPosition(0, 0);
	rect.setFillColor(sf::Color::Green);
	sprite.setTextureRect(sf::IntRect(0, 0, 417, 269));
}

Projectile::~Projectile()
{
}

void Projectile::update() {
	sprite.setPosition(rect.getPosition());
	rect.move(msProjectile, 0);
	
	counterLifeTime++;
	if (counterLifeTime >= lifeTime) {
		destroyProjectile();
	}
}

bool Projectile::isDestroy() {
	return destroy;
}

void Projectile::destroyProjectile() {
	destroy = true;
}

int Projectile::getAttackDmg() {
	return this->attackDmg;
}
void Projectile::setAttackDmg(int attackDmg) {
	this->attackDmg = attackDmg;
}
