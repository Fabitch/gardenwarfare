#include "stdafx.h"
#include "Noix.h"


Noix::Noix()
{
	rect.setSize(sf::Vector2f(35, 64));
	rect.setFillColor(sf::Color::Blue);
	sprite.setTextureRect(sf::IntRect(0, 0, 177, 189));
	sprite.setScale(0.5, 0.5);
}


Noix::~Noix()
{
}

int Noix::getHealth() {
	return health;
}

void Noix::setHealth(int health) {
	health = health;
}

void Noix::getHit(int dmg) {
	health -= dmg;
}

void Noix::update() {
	sprite.setPosition(rect.getPosition());
}

void Noix::setPosition(sf::Vector2f pos) {
	rect.setPosition(pos.x, pos.y);
}

bool Noix::isAlive() {
	return alive;
}

void Noix::die() {
	alive = false;
}