#pragma once
#include "Entity.h"

class Projectile : public Entity
{
private :
	float msProjectile = 10;
	int counterLifeTime =0;
	int lifeTime = 100;
	int attackDmg = 50;
public:
	Projectile();
	~Projectile();

	void update();
	bool isDestroy();
	void destroyProjectile();
	int getAttackDmg();
	void setAttackDmg(int attackDmg);

	bool destroy = false;
};

