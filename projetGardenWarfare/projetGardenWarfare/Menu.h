#pragma once
#include "SFML\Graphics.hpp"
#include <iostream>


#define MAX_NUMBER_OF_ITEM 3

using std::vector;

class Menu
{
public:
	Menu();
	~Menu();

	//Permet la destruction de l'instance Menu
	void MenuDestroy();

	//In the menu, the navigation is made with keyboard. Those two methods show to the user which button is selected
	void moveUp();
	void moveDown();

	//Put the buttons to the correct location and give a color, a text and a font style.
	void setPosition(float width, float height);

	//Give the position in the menu[] of the selected item to allow the moves.
	int getPressedItem() { return selectedItemIndex; }

	//Display the main menu on the window.
	void display(sf::RenderWindow &window, bool &game, bool &activeMenu, bool &about, bool &end);

private:
	int selectedItemIndex;
	sf::Font font;
	sf::Text menu[MAX_NUMBER_OF_ITEM];
	sf::Texture backgroundMenu;
	sf::Sprite sMenuBack;
};

