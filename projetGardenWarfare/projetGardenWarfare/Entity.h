#pragma once
#include "stdafx.h"
#include "SFML\Audio.hpp"
#include "SFML\Graphics.hpp"
#include <iostream>

class Entity
{
public:
	sf::Sprite sprite;
	sf::RectangleShape rect;
	sf::Text text;

	Entity();
	~Entity();
};

