#pragma once
#include <SFML\Graphics.hpp>
#include <iostream>
#include <sstream>

class EndGame
{
public:
	EndGame();
	~EndGame();
	
	//Permet la destruction de l'instance EndGame
	void EndGameDestroy();

	//Put the buttons to the correct location and give a color, a text and a font style.
	void setPosition();


	//Display the end game menu on the window and let the opportunity to come back to the main menu
	void display(sf::RenderWindow &window, bool &game, bool &activeMenu, bool &about, bool &end, int score);


private:
	sf::Font font;
	sf::Text text;
	sf::Text scoreTotal;
	sf::Text scoring;
	sf::Text back;
	sf::Texture backgroundMenu;
	sf::Sprite sMenuBack;
};

