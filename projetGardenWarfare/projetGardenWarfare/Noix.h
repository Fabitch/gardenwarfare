#pragma once
#include "Entity.h"

class Noix : public Entity
{
private:
	int health = 350;
	bool alive = true;
public:
	Noix();
	~Noix();

	int getHealth();
	void setHealth(int health);
	void getHit(int dmg);
	void update();
	void setPosition(sf::Vector2f pos);
	bool isAlive();
	void die();
	sf::Vector2f getPosition() { return rect.getPosition(); }
};

