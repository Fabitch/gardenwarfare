#include "stdafx.h"
#include "Peashooter.h"


Peashooter::Peashooter()
{
	rect.setSize(sf::Vector2f(35, 64));
	rect.setFillColor(sf::Color::Blue);
	sprite.setTextureRect(sf::IntRect(0, 0, 235, 240));
	sprite.setScale(0.3, 0.3);

	//Load Projectiles
	if (!spriteBullet.loadFromFile("../res/bullet.png")) {
		std::cout << "Error loading projectiles" << std::endl;
	}
	p1.sprite.setTexture(spriteBullet);
	p1.sprite.setScale(0.2, 0.2);

}

Peashooter::~Peashooter()
{
}

void Peashooter::display(sf::RenderWindow &window)
{
	//permet de lancer les projectiles
	if (clock.getElapsedTime().asSeconds() >= 2) {
		clock.restart();
		p1.rect.setPosition(rect.getPosition().x + rect.getSize().x / 2 -
			rect.getSize().x / 2, rect.getPosition().y + rect.getSize().y / 2 -
			rect.getSize().y / 1.80);
		projectileArray.push_back(p1);
	}
	//draw projectiles
	for (projectiles = 0;projectiles != projectileArray.size();projectiles++) {
		projectileArray[projectiles].update();
		window.draw(projectileArray[projectiles].sprite);
	}
}

void Peashooter::destroyProjectiles(sf::RenderWindow &window)
{
	//Delete projectiles who had been destroyed
	for (projectiles = 0; projectiles != projectileArray.size(); projectiles++)
	{
		if (projectileArray[projectiles].isDestroy() == true)
		{
			projectileArray.erase(projectileArray.begin() + projectiles);
			break;
		}
	}
}


void Peashooter::setPosition(sf::Vector2f pos)
{
	rect.setPosition(pos.x, pos.y);
}


void Peashooter::update() {
	sprite.setPosition(rect.getPosition());
}

int Peashooter::getHealth() {
	return health;
}
void Peashooter::setHealth(int health) {
	health = health;
}
void Peashooter::getHit(int dmg) {
	health -= dmg;
}

std::vector<Projectile> Peashooter::getProjectilesArray() {
	return projectileArray;
}

std::vector<Projectile>::size_type Peashooter::getProjectiles() {
	return projectiles;
}


void Peashooter::collision(Zombie &zombie) {
	for (projectiles = 0; projectiles != projectileArray.size(); projectiles++) {
		if (projectileArray[projectiles].rect.getGlobalBounds().intersects(zombie.rect.getGlobalBounds())) {
			projectileArray[projectiles].destroyProjectile();
			zombie.getHit(projectileArray[projectiles].getAttackDmg());
			if (zombie.getHealth() <= 0) {
				zombie.die();
			}
		}
	}
}

bool Peashooter::isAlive() {
	return alive;
}

void Peashooter::die() {
	alive = false;
}
