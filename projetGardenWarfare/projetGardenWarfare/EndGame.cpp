#include "stdafx.h"
#include "EndGame.h"


EndGame::EndGame()
{
	if (!font.loadFromFile("../res/arial.ttf"))
	{
		std::cout << "Le fichier arial.ttf n'existe pas" << std::endl;
	}

	//Load the background of the menu
	if (!backgroundMenu.loadFromFile("../res/endImage.jpeg")) {
		std::cout << "Error loading the background" << std::endl;
	}

	sMenuBack.setTexture(backgroundMenu);
}


EndGame::~EndGame()
{
}

void EndGame::setPosition()
{

	scoreTotal.setFont(font);
	scoreTotal.setFillColor(sf::Color::Black);
	scoreTotal.setCharacterSize(35);
	scoreTotal.setString("Score: ");
	scoreTotal.setPosition(sf::Vector2f(400, 310));

	scoring.setFont(font);
	scoring.setFillColor(sf::Color::Black);
	scoring.setCharacterSize(35);
	scoring.setPosition(sf::Vector2f(500, 310));

	back.setFont(font);
	back.setFillColor(sf::Color::Red);
	back.setString("�Back");
	back.setPosition(sf::Vector2f(5, 410));
}

void EndGame::EndGameDestroy()
{
	delete this;
}

void EndGame::display(sf::RenderWindow &window, bool &game, bool &activeMenu, bool &about, bool &end, int score)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::KeyReleased:
			if (event.key.code == sf::Keyboard::Return) {
				activeMenu = true;
				game = false;
				about = false;
				end = false;
			}
			break;
		case sf::Event::Closed:
			window.close();
			break;
		}
	}
	window.draw(sMenuBack);
	std::stringstream ss;
	ss << score;
	scoring.setString(ss.str().c_str());
	window.draw(scoreTotal);
	window.draw(scoring);
	window.draw(back);
}