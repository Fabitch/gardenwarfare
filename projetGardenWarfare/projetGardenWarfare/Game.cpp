#include "stdafx.h"
#include "Game.h"
#include <iostream>


Game::Game():window(sf::VideoMode(960, 480), "Garden Warfare") 
{
	window.setFramerateLimit(60);
}


Game::~Game()
{
	window.close();
}


void Game::init() 
{
	//Load the background
	if (!textBackground.loadFromFile("../res/backgroundGardenWarfare.png")) {
		std::cout << "Error loading the background" << std::endl;
	}

	sBoard.setTexture(textBackground);

	//Load Zombie

	if (!spriteSheetZombie.loadFromFile("../res/zombieWalk4.png")) {
		std::cout << "Error loading zombies" << std::endl;
	}
	zombie1.sprite.setTexture(spriteSheetZombie);

	//Load Peashooter

	if (!peashooterImage.loadFromFile("../res/PeaShooterImage.png")) {
		std::cout << "Error loading peashooters" << std::endl;
	}
	peashooter1.sprite.setTexture(peashooterImage);

	//load Noix
	if (!noixImage.loadFromFile("../res/wallNut.png")) {
		std::cout << "Error loading noix" << std::endl;
	}
	noix1.sprite.setTexture(noixImage);

}

void Game::run()
{
	init();
	processEvents();
}

void Game::processEvents()
{
	bool activeMenu = true, game = false, about = false, end = false;
	int dx, dy, score = 0;
	menu.setPosition(window.getSize().x, window.getSize().y);
	menuAbout.setPosition(window.getSize().x, window.getSize().y);
	endGame.setPosition();
	while (window.isOpen())
	{
		sf::Event event;
		//Display the main menu
		if (!game && !about && activeMenu && !end) 
		{
			window.clear();
			menu.display(window, game, activeMenu, about, end);
			score = 0;
		}
		//Destroy the main menu if it is activated at the same time of the about menu or the game
		else if((about || game) && activeMenu){
			menu.MenuDestroy();
		}
		//Display the about menu
		else if (!game && !activeMenu && about && !end) {
			window.clear();
			menuAbout.display(window, game, activeMenu, about);
		}
		//Display the end menu
		else if (game && !activeMenu && !about && end) {
			window.clear();
			endGame.display(window, game, activeMenu, about, end, score);
		}
		//Display the game
		else {
			while (window.pollEvent(event))
			{
				bool isPlace = true;
				sf::Vector2i pos = sf::Mouse::getPosition(window);
				sf::Vector2f newPos = sf::Vector2f(size*int(pos.x / size), size*int(pos.y / size));
				//make the peashooters spawning on the case of the board
				peashooter1.setPosition(newPos);
				//make the noix spawning on the case of the board	
				noix1.setPosition(newPos);

				switch (event.type)
				{
				case sf::Event::Closed:
					window.close();

				case sf::Event::KeyReleased:
					switch (event.key.code)
					{
					case sf::Keyboard::E:						
							//A peashooter can't be placed on another peashooter
							for (peashooters = 0; peashooters != peashooterArray.size(); peashooters++) {
								if (peashooter1.getPosition() == peashooterArray[peashooters].getPosition()) {
									isPlace = false;
								}
							}
							//A peashooter can't be placed on a wall-nut either
							for (noix = 0; noix != noixArray.size(); noix++) {
								if (peashooter1.getPosition() == noixArray[noix].getPosition()) {
									isPlace = false;
								}
								//Spawn of the first peashooter
								if (peashooterArray.size() == 0 && peashooter1.getPosition() != noixArray[noix].getPosition()) {
									peashooterArray.push_back(peashooter1);
								}
							}
						//take 2 secondes to spawn a peashooters
						if (clockSpawnPeashooters.getElapsedTime().asMilliseconds() >= spawnTimePeashooters) {
								clockSpawnPeashooters.restart();
							if (isPlace) {
								peashooterArray.push_back(peashooter1);
							}
						}
						break;
					case sf::Keyboard::R:				
						//A wall-nut can't be placed on a wall-nut 
						for (noix = 0; noix != noixArray.size();noix++) {
							if (noix1.getPosition() == noixArray[noix].getPosition()) {
								isPlace = false;
							}
						}
						//A wall-nut can't be placed on a peashooter either
						for (peashooters = 0; peashooters != peashooterArray.size(); peashooters++) {
							if (noix1.getPosition() == peashooterArray[peashooters].getPosition()) {
								isPlace = false;
							}
							//Spawn of the first wall-nut
							if (noixArray.size() == 0 && noix1.getPosition() != peashooterArray[peashooters].getPosition()) {
								noixArray.push_back(noix1);
							}
						}
						//take 2 secondes to spawn a peashooters
						if (clockSpawnWallNut.getElapsedTime().asMilliseconds() >= spawnTimeWallNut) {
							clockSpawnWallNut.restart();
							if (isPlace) {
								noixArray.push_back(noix1);
							}
						}
						break;
					}
					break;
				}
				
			}
			window.clear();

			//Delete zombies who are dead
			for (i = 0; i != zombieArray.size(); i++) {
				if (!zombieArray[i].isAlive()) {
					std::cout << "Zombie has been slain !" << std::endl;
					score++;
					zombieArray.erase(zombieArray.begin() + i);
					break;
				}
			}

			window.draw(sBoard);

			//add zombie to list of zombies
			if (clockZombieSpawn.getElapsedTime().asMilliseconds() >= spawnTimeZombie) {
				if (spawnTimeZombie > 350) {
					spawnTimeZombie -= 50;
				}
				clockZombieSpawn.restart();
				int random, y;
				srand(time(NULL));
				random = rand() % 5 + 1;
				switch (random)
				{
				default:
					y = 10;
					break;
				case 1:
					y = 10;
					break;
				case 2:
					y = 110;
					break;
				case 3:
					y = 210;
					break;
				case 4:
					y = 310;
					break;
				case 5:
					y = 410;
					break;

				}
				zombie1.rect.setPosition(900, y);
				zombieArray.push_back(zombie1);
			}
			

			
			for (i = 0; i != zombieArray.size(); i++) {
				//draw zombies
				zombieArray[i].update();
				zombieArray[i].updateMovement();
				window.draw(zombieArray[i].sprite);

				//Allow the zombie to walk again
				bool collisions = false;
				for (peashooters = 0; peashooters != peashooterArray.size(); peashooters++) {
					if (zombieArray[i].rect.getGlobalBounds().intersects(peashooterArray[peashooters].rect.getGlobalBounds())) {
						collisions = true;
					}
				}
				for (noix = 0; noix != noixArray.size(); noix++) {
					if (zombieArray[i].rect.getGlobalBounds().intersects(noixArray[noix].rect.getGlobalBounds())) {
						collisions = true;
					}
				}
				if (!collisions) {
					zombieArray[i].setMoving(true);
				}

				//If a zombie walk to the left of the window, a counter is incremented and the zombie dies
				if (zombieArray[i].rect.getPosition().x < 0)
				{
					countPassedZombie++;
					zombieArray[i].die();
				}
			}

			for (peashooters = 0; peashooters != peashooterArray.size(); peashooters++) {
				//draw peashooters
				peashooterArray[peashooters].update();
				window.draw(peashooterArray[peashooters].sprite);
				peashooterArray[peashooters].display(window);
				peashooterArray[peashooters].destroyProjectiles(window);

				//Collision between peashooter and zombie
				for (i = 0; i != zombieArray.size(); i++) {
					if (peashooterArray[peashooters].rect.getGlobalBounds().intersects(zombieArray[i].rect.getGlobalBounds())) {
						zombieArray[i].setMoving(false);
						if (zombieArray[i].getClockAsSeconds() >= zombieArray[i].getAttackSpeed()) {
							zombieArray[i].updateClockZombie();
							peashooterArray[peashooters].getHit(zombieArray[i].getAttackDmg());
							if (peashooterArray[peashooters].getHealth() <= 0) {
								zombieArray[i].setMoving(true);
								peashooterArray[peashooters].die();
							}
						}
					}
					//Collision between a projectile and a zombie
					peashooterArray[peashooters].collision(zombieArray[i]);
				}

				//Delete peashooter who is dead
				if (!peashooterArray[peashooters].isAlive()) {
					peashooterArray.erase(peashooterArray.begin() + peashooters);
					break;
				}
			}

			for (noix = 0;noix != noixArray.size();noix++) {
				//draw noix
				noixArray[noix].update();
				window.draw(noixArray[noix].sprite);

				//Collision between wall-nut and zombie
				for (i = 0; i != zombieArray.size(); i++) {
					if (noixArray[noix].rect.getGlobalBounds().intersects(zombieArray[i].rect.getGlobalBounds())) {
						zombieArray[i].setMoving(false);
						if (zombieArray[i].getClockAsSeconds() >= zombieArray[i].getAttackSpeed()) {
							zombieArray[i].updateClockZombie();
							noixArray[noix].getHit(zombieArray[i].getAttackDmg());
							std::cout << noixArray[noix].getHealth() << std::endl;
							if (noixArray[noix].getHealth() <= 0) {
								zombieArray[i].setMoving(true);
								noixArray[noix].die();
							}
						}
					}
				}

				//Delete wall-nut who is dead
				if (!noixArray[noix].isAlive()) {
					noixArray.erase(noixArray.begin() + noix);
					break;
				}
			}

			//Allow to show the end menu if two zombies arrived at the left of the window 
			//and clear the vectors for the next game.
			if (countPassedZombie >= 2) {
				end = true;
				score--;
				countPassedZombie = 0;
				peashooterArray.clear();
				noixArray.clear();
				zombieArray.clear();
				spawnTimeZombie = 3000;
			}
		}
		window.display();
	}
}

