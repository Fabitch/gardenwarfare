#pragma once
#include "Entity.h"
#include <ctime>

class Zombie : public Entity
{
private :
	int health = 150;
	float msZombie = 1;
	int counterWalking = 0;
	int attackDmg = 50;
	int attackSpeed = 2;//en secondes
	bool alive= true;
	bool moving=true;
	float frameTime = 1.0 / 60.0;
	float timePerSpriteZombie = 0.2;
	sf::Clock clockZombieAnimation;
	sf::Clock clockZombieAttack;


public:
	Zombie();
	~Zombie();


	Zombie* clone();
	void update();
	void updateMovement();
	void setHealth(int health);
	void getHit(int dmg);
	int getHealth();
	float getMs();
	void setMs(float ms);
	int getAttackDmg();
	void setAttackDmg(int dmg);
	bool isAlive();
	void setMoving(bool moov);
	void die();
	int getAttackSpeed();
	int getClockAsSeconds();
	void updateClockZombie();

};
