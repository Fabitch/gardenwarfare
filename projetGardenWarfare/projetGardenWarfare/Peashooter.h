#pragma once
#include "Entity.h"
#include "Projectile.h"
#include "Zombie.h"

class Peashooter : public Entity
{
private :
	int attackSpeed = 2;
	int health= 150;
	bool alive = true;
	std::vector<Projectile> projectileArray;
	std::vector<Projectile>::size_type projectiles;
	sf::Texture spriteBullet;
	Projectile p1;
	sf::Clock clock;
public:
	Peashooter();
	~Peashooter();

	int getHealth();
	void setHealth(int health);
	void getHit(int dmg);
	std::vector<Projectile> getProjectilesArray();
	std::vector<Projectile>::size_type getProjectiles();
	void update();
	void setPosition(sf::Vector2f pos);
	sf::Vector2f getPosition() { return rect.getPosition(); }
	void display(sf::RenderWindow &window);
	void destroyProjectiles(sf::RenderWindow &window);
	void collision(Zombie &zombie);
	bool isAlive();
	void die();
};


