#include "stdafx.h"
#include "MenuAbout.h"


MenuAbout::MenuAbout()
{
	if (!font.loadFromFile("../res/arial.ttf"))
	{
		std::cout << "Le fichier arial.ttf n'existe pas" << std::endl;
	}

	//Load the background of the menu
	if (!backgroundMenu.loadFromFile("../res/menuBackground.jpg")) {
		std::cout << "Error loading the background" << std::endl;
	}

	sMenuBack.setTexture(backgroundMenu);
}


void MenuAbout::setPosition(float width, float height) 
{
	text.setFont(font);
	text.setFillColor(sf::Color::Black);
	text.setString("Jeu r�alis� par Gaillat, Puche, \nSathiyananthan. �tudiants de \nla HELHa Campus Mons,\n dans le cadre du cours de \nMr.V.Altares.");
	text.setPosition(sf::Vector2f(270, 150));

	back.setFont(font);
	back.setFillColor(sf::Color::Red);
	back.setString("�Back");
	back.setPosition(sf::Vector2f(5, 410));
}


MenuAbout::~MenuAbout()
{
	std::cout << "Destruction about menu" << std::endl;
}

void MenuAbout::MenuAboutDestroy()
{
	delete this;
}

void MenuAbout::display(sf::RenderWindow &window, bool &game, bool &activeMenu, bool &about)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::KeyReleased:
			if(event.key.code == sf::Keyboard::Return){
				activeMenu = true;
				game = false;
				about = false;
			}
			break;
		case sf::Event::Closed:
			window.close();
			break;
		}
	}
	window.draw(sMenuBack);
	window.draw(text);
	window.draw(back);
}