#pragma once
#include "SFML\Graphics.hpp"
#include <iostream>

class MenuAbout
{
public:
	MenuAbout();
	~MenuAbout();

	//Permet la destruction de l'instance MenuAbout
	void MenuAboutDestroy();

	//Put the buttons to the correct location and give a color, a text and a font style.
	void setPosition(float width, float height);

	//Display the about menu on the window and let the opportunity to come back to the main menu
	void display(sf::RenderWindow &window, bool &game, bool &activeMenu, bool &about);

private:
	sf::Font font;
	sf::Text text;
	sf::Text back;
	sf::Texture backgroundMenu;
	sf::Sprite sMenuBack;
};

