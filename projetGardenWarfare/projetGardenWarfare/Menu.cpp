#include "stdafx.h"
#include "Menu.h"


Menu::Menu()
{
	if (!font.loadFromFile("../res/arial.ttf")) 
	{
		//HANDLE ERROR
	}

	//Load the background of the menu
	if (!backgroundMenu.loadFromFile("../res/menuBackground.jpg")) {
		std::cout << "Error loading the background" << std::endl;
	}

	sMenuBack.setTexture(backgroundMenu);

	selectedItemIndex = 0;
}

Menu::~Menu()
{
	std::cout << "Destruction menu" << std::endl;
}

void Menu::setPosition(float width, float height)
{
	menu[0].setFont(font);
	menu[0].setFillColor(sf::Color::Red);
	menu[0].setString("Play");
	menu[0].setPosition(sf::Vector2f(width / 2, height / (MAX_NUMBER_OF_ITEM + 1) * 1));

	menu[1].setFont(font);
	menu[1].setFillColor(sf::Color::Black);
	menu[1].setString("About");
	menu[1].setPosition(sf::Vector2f(width / 2, height / (MAX_NUMBER_OF_ITEM + 1) * 2));

	menu[2].setFont(font);
	menu[2].setFillColor(sf::Color::Black);
	menu[2].setString("Exit");
	menu[2].setPosition(sf::Vector2f(width / 2, height / (MAX_NUMBER_OF_ITEM + 1) * 3));
}

void Menu::MenuDestroy() 
{
	for (int i = 0; i < MAX_NUMBER_OF_ITEM;i++)
	{
		menu[i].setString("");
	}
	std::cout << "Destroyed" << std::endl;
	delete this;
}

void Menu::moveUp()
{
	if (selectedItemIndex - 1 >= 0)
	{
		menu[selectedItemIndex].setFillColor(sf::Color::Black);
		selectedItemIndex--;
		menu[selectedItemIndex].setFillColor(sf::Color::Red);
	}
}

void Menu::moveDown()
{
	if (selectedItemIndex + 1 < MAX_NUMBER_OF_ITEM)
	{
		menu[selectedItemIndex].setFillColor(sf::Color::Black);
		selectedItemIndex++;
		menu[selectedItemIndex].setFillColor(sf::Color::Red);
	}
}

void Menu::display(sf::RenderWindow &window, bool &game, bool &activeMenu, bool &about, bool &end)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::KeyReleased:
			switch (event.key.code)
			{
			case sf::Keyboard::Up:
				moveUp();
				break;

			case sf::Keyboard::Down:
				moveDown();
				break;

			case sf::Keyboard::Return:
				switch (getPressedItem())
				{
				case 0:
					activeMenu = false;
					game = true;
					about = false;
					end = false;
					break;
				case 1:
					activeMenu = false;
					game = false;
					about = true;
					break;
				case 2:
					window.close();
					break;
				}
				break;
			}
			break;
		case sf::Event::Closed:
			window.close();
			break;
		}
	}
	window.draw(sMenuBack);
	for (int i = 0; i < MAX_NUMBER_OF_ITEM;i++)
	{
		window.draw(menu[i]);
	}
}

