#pragma once
#include <stdlib.h>
#include <ctime>
#include <iostream>
#include <cstdlib>
#include <SFML/Graphics.hpp>
#include "Zombie.h"
#include "Entity.h"
#include "Peashooter.h"
#include "Projectile.h"
#include "Menu.h"
#include "MenuAbout.h"
#include "EndGame.h"
#include "Noix.h"

class Menu;

class Game
{
public:
	Game();
	~Game();

	//Start the game.
	void run();

private:

	//This method initialize the sprites, the textures.
	void init();
	//This method show the window and process the events.
	void processEvents();

private:
	sf::RenderWindow window;
	Menu menu;
	MenuAbout menuAbout;
	EndGame endGame;
	Zombie zombie1;
	Peashooter peashooter1;
	Noix noix1;
	int size = 100, countPassedZombie = 0;

	//Initialization of the Textures
	sf::Texture textBackground;
	sf::Texture spriteSheetZombie;
	sf::Texture peashooterImage;
	sf::Texture noixImage;

	//Initialization of the Sprites
	sf::Sprite sBoard;

	//Initialization of the clocks
	sf::Clock clockZombieSpawn;
	sf::Clock clockZombieAttack;
	sf::Clock clockSpawnPeashooters;
	sf::Clock clockSpawnWallNut;

	//zombie vector array 
	std::vector<Zombie> zombieArray;
	std::vector<Zombie>::size_type i;

	//creation of peashooters vector
	std::vector<Peashooter> peashooterArray;
	std::vector<Peashooter>::size_type peashooters;

	//creation of noix vector
	std::vector<Noix> noixArray;
	std::vector<Noix>::size_type noix;

	int spawnTimeZombie = 3000; // en milliseconds
	int spawnTimePeashooters = 2000;
	int spawnTimeWallNut = 5000;
};

