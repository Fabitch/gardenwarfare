#include "stdafx.h"
#include "Zombie.h"


Zombie::Zombie()
{
	rect.setSize(sf::Vector2f(64, 64));
	rect.setFillColor(sf::Color::Blue);
	sprite.setTextureRect(sf::IntRect(0, 64 * 2, 64, 64));
	sprite.setScale(1.3, 1.3);
}

Zombie::~Zombie()
{
}

Zombie* Zombie::clone() {
	return new Zombie(*this);
}


void Zombie::update() {
	sprite.setPosition(rect.getPosition());
	if (rect.getPosition().x < 0) {
		die();
	}
}

void Zombie::updateMovement() {
	rect.move(-msZombie, 0);
		if (clockZombieAnimation.getElapsedTime().asSeconds() > timePerSpriteZombie)
		{
			clockZombieAnimation.restart();
			sprite.setTextureRect(sf::IntRect(counterWalking * 64, 64 * 2, 64, 64));
			counterWalking = (counterWalking + 1) % 6;
		}
}

void Zombie::setHealth(int health) {

	this->health = health;
}

void Zombie::getHit(int dmg) {
	this->health -= dmg;
}

int Zombie::getHealth() {
	return health;
}

float Zombie::getMs() {
	return msZombie;
}

void Zombie::setMs(float ms) {
	msZombie = ms;
}

bool Zombie::isAlive() {
	return alive;
}

void Zombie::setMoving(bool mov) {
	int msBefore = msZombie;
	if (mov == true) {
		msZombie = 1.0;
	}
	else {
		msZombie = 0.;
	}
}

int Zombie::getAttackDmg() {
	return attackDmg;
}
void Zombie::setAttackDmg(int dmg) {
	attackDmg = dmg;
}

void Zombie::die() {
	alive = false;
}

int Zombie::getAttackSpeed() {
	return attackSpeed;
}

int Zombie::getClockAsSeconds() {
	return clockZombieAttack.getElapsedTime().asSeconds();
}

void Zombie::updateClockZombie() {
	clockZombieAttack.restart();
}

